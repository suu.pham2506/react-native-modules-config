import { Navigation } from 'react-native-navigation';
import Login from '../screens/User/Login';
import NativeToast from '../screens/User/NativeToast';
import NativeElement from '../screens/User/NativeElement';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('login', () => Login, store, Provider);
  Navigation.registerComponent(
    'nativeToast',
    () => NativeToast,
    store,
    Provider
  );
  Navigation.registerComponent(
    'nativeElement',
    () => NativeElement,
    store,
    Provider
  );
}
