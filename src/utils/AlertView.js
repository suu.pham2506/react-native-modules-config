import { Alert } from 'react-native';
import { store } from '../Setup';
import LoginActions from '../redux/LoginRedux/actions';

export function showAlertError(error) {
  if (error) {
    if (error.code === 401) {
      if (store.getState().user.isLogined) {
        store.dispatch(LoginActions.logout());
        Alert.alert('The Books', 'Phiên đăng nhập đã hết hạn.');
      }
      Alert.alert('The Books', 'Phiên đăng nhập đã hết hạn.');
    } else if (error.message === null || error.message === '') {
      Alert.alert('The Books', 'Có lỗi xảy ra. Hãy thử lại sau!');
    } else {
      Alert.alert('The Books', `${error.message}`);
    }
  }
}
