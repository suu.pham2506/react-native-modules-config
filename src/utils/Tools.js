import _ from 'lodash';

export default {
  validateField(name) {
    return name !== '';
  },

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },

  validatePhone(phone) {
    const regex = /^0(1\d{9}|8\d{8}|9\d{8})$/;
    return regex.test(phone);
  },

  validateName(name) {
    const regex = /^[^-=@#%&*“‘`~!^|<>():;_.,+[\]\\0-9]+$/;
    return regex.test(name);
  },

  getAuthorName(authors) {
    let authorsName = '';
    if (authors && authors.length > 0) {
      _.map(authors, (item) => {
        if (_.isEmpty(authorsName)) {
          authorsName = item.Name;
        } else {
          authorsName = `${authorsName}, ${item.Name}`;
        }
      });
    }
    return authorsName;
  },
};
