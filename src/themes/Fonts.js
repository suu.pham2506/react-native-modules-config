const font = {
  text: 'System',
  note: 'System',
  title: 'System',
  header: 'System',
};

const size = {
  tiny: 8.5,
  semiSmall: 10,
  small: 12,
  xsmall: 13,
  normal: 15,
  large: 17,
  xlarge: 18,
  xxlarge: 20,
  titleN: 24,
  titleL: 26,
  titleM: 30,
  titleXL: 36,
  header: 40,
};

export { size, font };
