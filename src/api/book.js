import { get, post, del, put } from './utils';
import { store } from '../Setup';

export async function book() {
  return get('/api/Cms/Home');
}

export async function search(url) {
  return get(`/api/Books/${url}`);
}

export async function searchSuggestion(query) {
  return get(`/api/Search/Suggestion?query=${query}`);
}

export async function getBookById(bookId) {
  return get(`/api/Books/${bookId}`);
}

export async function getBookRelatedById(bookId) {
  return get(`/api/Books/${bookId}/RelatedBooks`);
}

export async function followBook(data) {
  const userId = store.getState().user.data.Id;
  return post(`/api/Users/${userId}/Favorite`, data);
}

export async function unFollowBook(data) {
  const userId = store.getState().user.data.Id;
  return post(`/api/Users/${userId}/RemoveFavorite`, data);
}

export async function getListReviewsById(bookId, pageSize, pageNumber) {
  return get(`/api/Reviews?bookId=${bookId}&pageSize=${pageSize}&pageNumber=${pageNumber}`);
}

export async function reviewBook(data) {
  return post('/api/Reviews', data);
}

export async function getFollowingBooks() {
  const userId = store.getState().user.data.Id;
  return get(`/api/users/${userId}/followingbooks`);
}

export async function requestBook(data) {
  const userId = store.getState().user.data.Id;
  return post(`/api/users/${userId}/requestbook`, data);
}

export async function editReview(data) {
  const newData = {
    BookId: data.BookId,
    UserId: data.UserId,
    Content: data.Content,
    StarRating: data.StarRating,
  };
  return put(`/api/Reviews/${data.CommentId}`, newData);
}

export async function deleteReview(commentId) {
  return del(`/api/Reviews/${commentId}`);
}

export async function getListWaitingBooks() {
  const userId = store.getState().user.data.Id;
  return get(`/api/users/${userId}/waitingbooks`);
}

export async function waitingBook(data) {
  const newData = {
    UserId: data.UserId,
  };
  return post(`/api/books/${data.BookId}/addwaiting`, newData);
}

export async function deleteWaitingBook(data) {
  const newData = {
    UserId: data.UserId,
  };
  return del(`/api/books/${data.BookId}/deletewaiting`, newData);
}
