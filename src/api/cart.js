import { get, post, del, put } from './utils';
import { store } from '../Setup';

export async function getListCart() {
  const cartId = store.getState().cart.id;
  return get(`/api/Basket/${cartId}`);
}

export async function addToListCart(data) {
  return post('/api/Basket', data);
}

export async function deleteItemFromListCart(data) {
  return del('/api/Basket', data);
}

export async function updateItemQuantityInCart(data) {
  const cartId = store.getState().cart.id;
  return put(`/api/Basket/${cartId}`, data);
}

export async function checkOut() {
  const data = {
    ShippingAddress: '',
    ShippingRequired: false,
    Note: '',
    UserId: store.getState().user.data.Id,
  };
  return post('/api/Orders', data);
}
