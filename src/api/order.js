import { get, post, del, put } from './utils';
import { store } from '../Setup';

export async function getListOrder() {
  const userId = store.getState().user.data.Id;
  return get(`/api/Orders?userId=${userId}`);
}

export async function checkOut() {
  const data = {
    ShippingAddress: '',
    ShippingRequired: false,
    Note: '',
    UserId: store.getState().user.data.Id,
  };
  return post('/api/Orders', data);
}

export async function changeOrderStatus(data) {
  const newData = {
    OrderItemIds: data.OrderItemIds,
    OrderItemStatus: data.OrderItemStatus,
  };
  return put(`/api/Orders/${data.orderId}/setstatus`, newData);
}
