import { takeLatest, take } from 'redux-saga/effects';
/* ------------- Types ------------- */
import { LoginTypes } from './LoginRedux/actions';
/* ------------- Sagas ------------- */
import { loginSaga, logoutSaga, registerSaga } from './LoginRedux/saga';

/* ------------- Connect Types To Sagas ------------- */
export default function* root() {
  yield [
    // some sagas only receive an action
    takeLatest(LoginTypes.USER_LOGIN, loginSaga),
    takeLatest(LoginTypes.USER_REGISTER, registerSaga),
    takeLatest(LoginTypes.USER_LOGOUT, logoutSaga),
  ];
}
