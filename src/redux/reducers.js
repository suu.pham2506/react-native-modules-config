import { combineReducers } from 'redux';
import UserReducer from './UserRedux/reducer';
import LoginReducer from './LoginRedux/reducer';

export default combineReducers({
  user: UserReducer,
  login: LoginReducer,
});
