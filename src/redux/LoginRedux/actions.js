import { createAction } from '../../utils/ReduxUtils';

export const LoginTypes = {
  USER_LOGIN: 'USER_LOGIN',
  USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
  USER_LOGIN_FAILURE: 'USER_LOGIN_FAILURE',
  USER_REGISTER: 'USER_REGISTER',
  USER_REGISTER_SUCCESS: 'USER_REGISTER_SUCCESS',
  USER_REGISTER_FAILURE: 'USER_REGISTER_FAILURE',
  USER_LOGOUT: 'USER_LOGOUT',
};

const login = data => createAction(LoginTypes.USER_LOGIN, { data });
const loginSuccess = (response, role) =>
  createAction(LoginTypes.USER_LOGIN_SUCCESS, { response, role });
const loginFailure = err => createAction(LoginTypes.USER_LOGIN_FAILURE, { err });
const register = data => createAction(LoginTypes.USER_REGISTER, { data });
const registerSuccess = response => createAction(LoginTypes.USER_REGISTER_SUCCESS, { response });
const registerFailure = err => createAction(LoginTypes.USER_REGISTER_FAILURE, { err });
const logout = data => createAction(LoginTypes.USER_LOGOUT, { data });

export default {
  login,
  loginSuccess,
  loginFailure,
  register,
  registerSuccess,
  registerFailure,
  logout,
};
