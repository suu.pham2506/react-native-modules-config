import { put, call, select } from 'redux-saga/effects';
import LoginActions from './actions';
import { store } from '../../Setup';
import { login, register } from '../../api/auth';

export function* loginSaga({ data }) {
  try {
    const response = yield call(login, data);
    yield put(LoginActions.loginSuccess(response));
  } catch (error) {
    yield put(LoginActions.loginFailure(error));
  }
}

export function* registerSaga({ data }) {
  try {
    const response = yield call(register, data);
    yield put(LoginActions.registerSuccess(response));
  } catch (error) {
    yield put(LoginActions.registerFailure(error));
  }
}

export function* logoutSaga() {}
