import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TextInput, Platform } from 'react-native';
import { Note } from './Text';
import { Colors } from '../themes';

const TEXT_IMPUT = 'TEXT_INPUT';

const RCTKeyboardToolbarTextInput = require('react-native-textinput-utils');

const { width } = Dimensions.get('window');
class TextInputWithTitle extends Component {
  onFocus() {
    this.refs.TEXT_INPUT.focus();
  }

  renderTextInput(type) {
    if (type !== 'nomal' && Platform.OS === 'ios') {
      return (
        <RCTKeyboardToolbarTextInput
          {...this.props}
          ref={TEXT_IMPUT}
          style={styles.textInput}
          numberOfLines={1}
          keyboardType="numeric"
          rightButtonText={type}
          placeholderTextColor={Colors.greyLight}
        />
      );
    }
    return (
      <TextInput
        {...this.props}
        ref={TEXT_IMPUT}
        style={styles.textInput}
        numberOfLines={1}
        underlineColorAndroid="transparent"
        placeholderTextColor={Colors.greyLight}
      />
    );
  }

  render() {
    return (
      <View>
        <View style={styles.subContainer}>
          <Note style={styles.text1}>{this.props.title}</Note>
          {this.renderTextInput(this.props.numberTextInput ? this.props.numberTextInput : 'nomal')}
        </View>
        <View style={{ backgroundColor: '#e9e9e9', height: 1, marginHorizontal: 20 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  subContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 47,
    width,
    marginLeft: 20,
    marginRight: 10,
  },
  text1: {
    color: Colors.grey,
    width: 110,
  },
  textInput: {
    color: Colors.darkGrey,
    fontFamily: 'SVN-ProximaNovaLight',
    fontSize: 15,
    height: Platform.OS === 'ios' ? 18 : 40,
    flex: 1,
    paddingRight: 20,
  },
});

export default TextInputWithTitle;
