import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Colors } from '../themes';


const TextVertical = (props) => {
  return (
    <View style={styles.textContainer}>
      <Text style={styles.firstText}>{props.firstText}</Text>
      <Text style={styles.secondText}>{props.secondText}</Text>
    </View>
  );
};

export default TextVertical;

const styles = StyleSheet.create({
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  firstText: {
    fontSize: 10,
    color: Colors.default,
  },
  secondText: {
    fontSize: 14,
    color: Colors.default,
    fontWeight: 'bold',
    paddingTop: 3,
  },
});
