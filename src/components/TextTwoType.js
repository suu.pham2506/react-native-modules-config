import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { font, size } from '../themes/Fonts';
import appStyle from '../themes/AppStyle';
import { Text } from '../components/Text';
import { Colors } from '../themes';

const { height } = Dimensions.get('window');
const ratioHeight = height / 667;


const TextTwoType = (props) => {
  return (
    <View style={[appStyle.horizontal, { marginBottom: props.marginBottom * ratioHeight }]}>
      <Text style={[styles.firstText, { color: props.color }]}>{props.firstText}</Text>
      <Text style={styles.secondText}>{props.secondText}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  firstText: {
    fontSize: size.xxlarge,
    fontFamily: font.header,
  },
  secondText: {
    color: Colors.grey,
    fontSize: size.xxlarge,
    fontFamily: font.note,
  },
});

export default TextTwoType;
