import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Dimensions } from 'react-native';
import { Text } from '../components/Text';

const { width } = Dimensions.get('window');

class TextInputMultipleLine extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  onFocus() {
    this.textInputRef.focus();
  }
  render() {
    return (
      <View>
        <Text style={styles.title}>{this.props.title} {this.props.required ? <Text style={{ color: 'red' }}>*</Text> : ''}</Text>
        <TextInput
          {...this.props}
          ref={(ref) => {
            this.textInputRef = ref;
          }}
          editable
          placeholderTextColor="#ababab"
          underlineColorAndroid="transparent"
          style={[styles.textInput, { height: this.props.height }]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#e9e9e9',
    fontFamily: 'SVN-ProximaNovaLight',
    fontSize: 16,
    color: '#ababab',
    width: width - 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    color: '#262626',
    marginBottom: 10,
    marginTop: 20,
  },
});

export default TextInputMultipleLine;
