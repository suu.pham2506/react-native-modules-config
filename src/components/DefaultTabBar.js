import React from 'react';
import { ViewPropTypes, StyleSheet, View, Animated } from 'react-native';
import PropTypes from 'prop-types';
import Touchable from './Touchable';
import { Colors } from '../themes';
import { SubText } from './Text';

class DefaultTabBar extends React.Component {
  static propTypes = {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    fontFamily: PropTypes.string,
    tabStyle: ViewPropTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style,
  };

  static defaultProps = {
    activeTextColor: '#ffffff',
    inactiveTextColor: 'rgba(255, 255, 255, 0.7)',
    backgroundColor: null,
    fontFamily: 'SVN-ProximaNovaBold',
  };

  renderTabOption(name, page) {}

  renderTab = (name, page, isTabActive, onPressHandler) => {
    return (
      <Touchable
        style={styles.flexOne}
        key={name}
        accessible
        accessibilityLabel={name}
        accessibilityTraits="button"
        onPress={() => onPressHandler(page)}
      >
        <View style={[styles.tab, this.props.tabStyle]}>
          <SubText
            style={{
              color: isTabActive ? this.props.activeTextColor : this.props.inactiveTextColor,
              fontFamily: this.props.fontFamily,
              fontSize: 13,
            }}
          >
            {name}
          </SubText>
        </View>
      </Touchable>
    );
  };

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: 50,
      height: 3,
      backgroundColor: this.props.underLineColor ? this.props.underLineColor : Colors.default,
      bottom: 8,
    };

    const offset = (containerWidth / numberOfTabs - 50) / 2;
    const left = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0 + offset, containerWidth / numberOfTabs + offset],
    });

    return (
      <View
        style={[styles.tabs, { backgroundColor: this.props.backgroundColor }, this.props.style]}
      >
        {this.props.tabs.map((name, page) => {
          const isTabActive = this.props.activeTab === page;
          const renderTab = this.props.renderTab || this.renderTab;
          return renderTab(name, page, isTabActive, this.props.goToPage);
        })}
        <Animated.View style={[tabUnderlineStyle, { left }, this.props.underlineStyle]} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  flexOne: {
    flex: 1,
  },
  tabs: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderBottomWidth: 1,
    borderColor: Colors.transparent,
  },
});

module.exports = DefaultTabBar;
