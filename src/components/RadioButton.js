import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';

import PropTypes from 'prop-types';
import { Note } from '../components/Text';

export default class RadioButton extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedIndex: nextProps.selectedIndex,
    });
  }

  getRadioStyle() {
    return {
      height: this.context.size,
      width: this.context.size,
      borderRadius: this.context.size / 2,
      borderWidth: this.context.thickness,
      borderColor:
        this.props.isSelected && this.props.activeColor
          ? this.props.activeColor
          : this.context.color,
    };
  }

  getRadioDotStyle() {
    return {
      height: this.context.size / 2,
      width: this.context.size / 2,
      borderRadius: this.context.size / 2,
      backgroundColor: this.props.color || this.props.activeColor,
    };
  }

  isSelected() {
    if (this.props.isSelected) {
      return <View style={this.getRadioDotStyle()} />;
    }
  }
  render() {
    const { children } = this.props;
    return (
      <View style={{ opacity: this.props.disabled ? 0.4 : 1 }}>
        <TouchableWithoutFeedback
          disabled={this.props.disabled}
          onPress={() => this.context.onSelect(this.props.index, this.props.value)}
        >
          <View
            style={[
              styles.container,
              this.props.style,
              this.props.isSelected ? { backgroundColor: this.context.highlightColor } : null,
            ]}
          >
            <View style={[styles.radio, this.getRadioStyle()]}>{this.isSelected()}</View>
            <View style={styles.item}>
              <Note style={styles.textItem}>{children}</Note>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

RadioButton.contextTypes = {
  onSelect: PropTypes.func.isRequired,
  size: PropTypes.number.isRequired,
  thickness: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  highlightColor: PropTypes.string,
};

let styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  radio: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    marginLeft: 9,
  },
  textItem: {
    color: '#4a4a4a',
  },
});
