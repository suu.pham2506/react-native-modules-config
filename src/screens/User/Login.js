import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Alert,
  Button,
  Platform,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Container from '../../components/Container';
import { Text } from '../../components/Text';
import SmoothPinCodeInput from '../../components/SmoothPinCodeInput';
import AnimatedMessage from '../../components/NativeElements/animated-message';
import AnimatedButton from '../../components/NativeElements/animated-button';
import AnimationProgressBar from '../../components/NativeElements/animated-progressbar';
const colors =
  Platform.OS === 'ios'
    ? {
        blue: '#007aff',
        gray: '#d8d8d8',
        green: '#4cd964',
        red: '#ff3b30',
        white: '#ffffff'
      }
    : {
        blue: '#4285f4',
        gray: '#d8d8d8',
        green: '#0f9d58',
        red: '#db4437',
        white: '#ffffff'
      };

const { width } = Dimensions.get('window');

class Login extends Component {
  static navigatorStyle = {
    navBarHidden: true
  };

  state = {
    code: '',
    password: '',
    progress: 0
  };

  _checkCode = code => {
    if (code !== '250697') {
      this.pinInput.shake().then(() => this.setState({ code: '' }));
    } else {
      this.message.showMessage('Sign in successfully', 1000);
      this.setState({ code: '' });
    }
  };

  render() {
    const { code, password } = this.state;
    setTimeout(
      function() {
        this.setState({ progress: this.state.progress + 20 * 0.5 });
      }.bind(this),
      1000
    );
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <Text style={styles.title}>Default</Text>
          <SmoothPinCodeInput
            ref={ref => {
              this.pinInput = ref;
            }}
            value={code}
            codeLength={6}
            onTextChange={code => this.setState({ code })}
            onFulfill={this._checkCode}
            onBackspace={() => console.log('No more back.')}
          />
        </View>
        <Button
          title="Show top"
          color="green"
          onPress={() => this.message.showMessage('Sign in successfully', 1000)}
        />
        <AnimatedMessage
          ref={message => (this.message = message)}
          messageType={'success'}
        />
        <AnimationProgressBar
          width={width - 20}
          height={10}
          value={this.state.progress}
          backgroundColorOnComplete={colors.blue}
          backgroundColor={colors.red}
          onComplete={() => {
            Alert.alert('Hey!', 'onComplete event fired!');
          }}
        />
        {/* <AnimatedButton
          foregroundColor={colors.green}
          label="Submit"
          onPress={() => this.b1.success()}
          ref={ref => (this.b1 = ref)}
          successIcon="check"
        />

        <AnimatedButton
          foregroundColor={colors.blue}
          label="Retweet"
          onPress={() => this.b2.success()}
          ref={ref => (this.b2 = ref)}
          successIcon="retweet"
        />

        <AnimatedButton
          foregroundColor={colors.red}
          label="Favorite"
          onPress={() => this.b3.success()}
          ref={ref => (this.b3 = ref)}
          successIcon="heart"
        />
        <AnimatedButton
          errorBackgroundColor={colors.red}
          errorIcon="thumbs-down"
          expandOnFinish
          foregroundColor={colors.blue}
          label="Am I even?"
          onPress={() =>
            new Date().getSeconds() % 2 === 0
              ? this.b4.success()
              : this.b4.error()
          }
          ref={ref => (this.b4 = ref)}
          successBackgroundColor={colors.green}
          successIcon="thumbs-up"
          width={240}
        />

        <AnimatedButton
          errorBackgroundColor={colors.red}
          errorIcon="thumbs-down"
          expandOnFinish
          foregroundColor={colors.blue}
          label="Am I even?"
          onPress={() =>
            new Date().getSeconds() % 2 === 0
              ? this.b5.success()
              : this.b5.error()
          }
          ref={ref => (this.b5 = ref)}
          successBackgroundColor={colors.green}
          successIcon="thumbs-up"
          width={240}
        />
        <AnimatedButton
          backgroundColor={colors.blue}
          errorBackgroundColor={colors.red}
          errorForegroundColor={colors.white}
          errorIcon="warning"
          foregroundColor={colors.white}
          label="Simulate an error"
          onPress={() => this.b6.error()}
          ref={ref => (this.b6 = ref)}
          shakeOnError
          width={240}
        />

        <AnimatedButton
          backgroundColor={colors.blue}
          foregroundColor={colors.white}
          label="Smile at me"
          onPress={() => this.b7.success()}
          ref={ref => (this.b7 = ref)}
          scaleOnSuccess
          successBackgroundColor={colors.green}
          successForegroundColor={colors.white}
          successIcon="smile-o"
          width={240}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  section: {
    alignItems: 'center',
    margin: 16
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8
  }
});

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
