import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Avatar,
  Badge,
  Button,
  ButtonGroup,
  CheckBox,
  Divider,
  Header,
  Input,
  ListItem,
  Slider,
  SocialIcon
} from '../../components/NativeElements';

const { width } = Dimensions.get('window');
export class NativeElement extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 2,
      checked: false,
      value: 0
    };
    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex });
    console.log('select', selectedIndex);
  }

  render() {
    const buttons = ['Hello', 'World', 'Buttons'];
    const { selectedIndex } = this.state;
    return (
      <View
        style={{
          marginTop: 20,
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1
        }}
      >
        {/* <Input
          placeholder="INPUT WITH CUSTOM ICON"
          leftIcon={<Icon name="user" size={24} color="black" />}
        />
        <Input placeholder="INPUT WITH SHAKING EFFECT" shake={true} />
        <Input
          placeholder="INPUT WITH ERROR MESSAGE"
          errorStyle={{ color: 'red' }}
          errorMessage="ENTER A VALID ERROR HERE"
        /> */}
        {/* <Avatar
          size="xlarge"
          rounded
          source={{
            uri:
              'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'
          }}
          onPress={() => console.log('Works!')}
          activeOpacity={0.7}
        />
        <Divider style={{ backgroundColor: 'blue', height: 1 }} />
        <Badge
          onPress={() => {
            console.log('pressed');
          }}
          value="5"
        />
        <Divider style={{ backgroundColor: 'blue', height: 1 }} />
        <Button
          title="LOADING BUTTON"
          raise
          loading
          loadingProps={{ size: 'large', color: 'rgba(111, 202, 186, 1)' }}
          titleStyle={{ fontWeight: '700' }}
          buttonStyle={{
            backgroundColor: 'green',
            width: 300,
            height: 45,
            borderColor: 'transparent',
            borderWidth: 0,
            borderRadius: 5
          }}
          containerStyle={{ marginTop: 20 }}
        />
        <Divider style={{ backgroundColor: 'blue', height: 1 }} />
        <ButtonGroup
          onPress={this.updateIndex}
          selectedIndex={selectedIndex}
          buttons={buttons}
          containerStyle={{ height: 100 }}
        />
        <Divider style={{ backgroundColor: 'red', height: 1 }} />
        <CheckBox
          title="Click Here"
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checked={this.state.checked}
          onPress={() => this.setState({ checked: !this.state.checked })}
        />
        <Header
          leftComponent={{ icon: 'menu', color: '#fff' }}
          centerComponent={{ text: 'MY TITLE', style: { color: '#fff' } }}
          rightComponent={{ icon: 'home', color: '#fff' }}
          outerContainerStyles={{
            backgroundColor: '#3D6DCC',
            width,
            justifyContent: 'center'
          }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}
        {/* {list.map((l, i) => (
          <ListItem
            key={i}
            leftAvatar={{ source: { uri: l.avatar_url } }}
            title={l.name}
            subtitle={l.subtitle}
          />
        ))} */}

        <Slider
          value={this.state.value}
          onValueChange={value => this.setState({ value })}
          maximumValue={20}
          minimumValue={0}
          step={2}
          minimumTrackTintColor="#1fb28a"
          maximumTrackTintColor="#d3d3d3"
          thumbTintColor="#1a9274"
          style={{ width: width - 20 }}
        />
        <Text>Value: {this.state.value}</Text>
        <SocialIcon
          title="Sign In With Facebook"
          button
          type="facebook"
          style={{ width: width - 20 }}
          loading
        />
        <SocialIcon
          title="Some Twitter Message"
          button
          type="twitter"
          style={{ width: width - 20 }}
          loading
        />
        <SocialIcon
          title="Some Youtube Message"
          button
          type="youtube"
          style={{ width: width - 20 }}
          loading
          onPress={() => alert('hihi')}
        />
        <SocialIcon
          title="Some Medium Message"
          button
          type="medium"
          style={{ width: width - 20 }}
          loading
        />
        <SocialIcon
          title="Some Gitlab Message"
          button
          type="gitlab"
          style={{ width: width - 20 }}
          loading
        />
        <SocialIcon
          title="Some Github Message"
          button
          type="github"
          style={{ width: width - 20 }}
          loading
        />
        <SocialIcon
          title="Some Instagram Message"
          button
          type="instagram"
          style={{ width: width - 20 }}
          loading
        />
      </View>
    );
  }
}

const list = [
  {
    name: 'Amy Farha',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
];
const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NativeElement);
